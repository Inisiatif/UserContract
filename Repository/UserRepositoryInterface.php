<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\User\Repository;

use Inisiatif\Component\Contract\User\Model\UserInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface UserRepositoryInterface
{
    /**
     * @param string|integer|null $id
     *
     * @return UserInterface|null
     */
    public function findById($id): ?UserInterface;

    /**
     * @param string $email
     *
     * @return UserInterface|null
     */
    public function findOneByEmail(string $email): ?UserInterface;
}
