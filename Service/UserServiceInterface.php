<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\User\Service;

use DateTimeInterface;
use Inisiatif\Component\Contract\User\Model\UserInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface UserServiceInterface
{
    /**
     * @param UserInterface $user
     * @return UserInterface
     */
    public function save(UserInterface $user): UserInterface;

    /**
     * @param UserInterface $user
     * @param string $newPassword
     * @return bool
     */
    public function changePassword(UserInterface $user, string $newPassword): bool;

    /**
     * @param UserInterface $user
     * @param DateTimeInterface $date
     * @return bool
     */
    public function updateLastLogin(UserInterface $user, DateTimeInterface $date): bool;
}
