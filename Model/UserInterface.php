<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\User\Model;

use DateTimeInterface;
use Inisiatif\Component\Contract\Resource\Model\ResourceInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface UserInterface extends ResourceInterface
{
    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @param string|null $value
     * @return self|UserInterface
     */
    public function setName(?string $value): self;

    /**
     * @return string|null
     */
    public function getEmail(): ?string;

    /**
     * @param string|null $value
     * @return self|UserInterface
     */
    public function setEmail(?string $value): self;

    /**
     * @return string|null
     */
    public function getPassword();

    /**
     * @param string $value
     * @return self|UserInterface
     */
    public function setPassword(string $value);

    /**
     * @return DateTimeInterface|null
     */
    public function getLastLogin(): ?DateTimeInterface;

    /**
     * @param DateTimeInterface|null $value
     * @return self|UserInterface
     */
    public function setLastLogin(?DateTimeInterface $value): self;

    /**
     * @return DateTimeInterface|null
     */
    public function getChangePasswordAt(): ?DateTimeInterface;

    /**
     * @param DateTimeInterface|null $value
     * @return self|UserInterface
     */
    public function setChangePasswordAt(?DateTimeInterface $value): self;
}
