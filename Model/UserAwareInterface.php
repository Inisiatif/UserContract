<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\User\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface UserAwareInterface
{
    /**
     * @return UserInterface|null
     */
    public function getUser(): ?UserInterface;

    /**
     * @param UserInterface|null $user
     * @return UserAwareInterface|self
     */
    public function setUser(?UserInterface $user);
}
